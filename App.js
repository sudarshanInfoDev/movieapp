import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import React, { useEffect } from 'react';
import 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Provider, useDispatch, useSelector } from 'react-redux';
import store from './store';
import { getDataFromStorage, saveToStorage } from './utils/asyncStorage';
import Home from './views/Home';
import ShortList from './views/ShortList';

const NavigationCOmponent = () => {
  const dispatch = useDispatch()
  const Tab = createBottomTabNavigator();
  const playlist = useSelector(state => state.playlistData.movies)

  useEffect(() => {
    if(playlist.length){
      console.log("set*-*-*-*-", playlist)
      saveToStorage(playlist)
    }
  }, [playlist])

  useEffect(() => {
    getDataFromStorage(dispatch)
  }, [])

  return (
    <NavigationContainer>
      <Tab.Navigator
        tabBarOptions={{
          activeTintColor: "seagreen",
          inactiveTintColor: "gray",
          labelStyle: {
            fontSize: 15,
          },
        }}
      >
        <Tab.Screen name="Movie List" component={Home}
          options={{
            tabBarLabel: 'Movies',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="movie-filter" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="Shortlist" component={ShortList}
          options={{
            tabBarLabel: 'Playlist',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="format-list-checks" color={color} size={size} />
            ),
            tabBarBadge: playlist.length,
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  )
}

const App = () => {
  return (
    <Provider store={store}>
      <NavigationCOmponent />
    </Provider >

  );
};

export default App;
