
import React from 'react';
import { Dimensions } from 'react-native';
import styled from 'styled-components/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const { width } = Dimensions.get("window");

const HeaderWrapper = styled.View`
    background-color: #fff;
    padding: 10px;
    margin: 10px;
    border-radius: 10px;
    display: flex;
    flex-direction:row;
`;

const HeaderText = styled.Text`
    font-size: 18px;
    margin-left: 8px;
    font-weight:bold;
`;


const Header = ({ title, icon }) => {
    return (
        <HeaderWrapper>
            <MaterialCommunityIcons name={`${icon ? icon : "movie-filter"}`} size={25} />
            <HeaderText>{title || "MOVIE APP"}</HeaderText>
        </HeaderWrapper>
    );
};

export default Header;
