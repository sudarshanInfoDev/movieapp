
import React from 'react';
import {
    Dimensions
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import styled from 'styled-components/native';


const { width, height } = Dimensions.get("window");

const CardWrapper = styled.View`
    flex-basis: 48%;
    background-color: #ffffff;
    height: ${height / 2.55}px;
    margin: 5px 0px;
    border-radius:10px;
    padding: 10px;
    border:1.5px dotted lightgray;
`;

const MovieThumbnail = styled.Image`
    height: 50%;
`;

const MovieTitle = styled.Text`
    font-size: 17px;
    text-align:left;
    margin:5px 5px;
    font-weight:bold;
    height: 15%;
`
const MovieDetail = styled.View`
    display: flex;
    flex-direction: row;
    align-items:center;
    margin-bottom:5px;
    /* justify-content:center; */
`;

const ReleaseYear = styled.Text`
    text-align:left;
    margin:0px 5px;
    font-weight:bold;
    font-size:15px;
`;
const Director = styled.Text`
    text-align:left;
    margin:0px 5px;
    font-weight:bold;
    font-size:15px;

`;
const AddToFavouraite = styled.TouchableOpacity`
    display:flex;
    flex-direction:row;
    align-items:center;
    border: 1px solid seagreen;
    border-radius: 50px;
`;

const AddToFavouraiteText = styled.Text`
font-weight:bold;
color: seagreen;
   

`;

const MoviesCard = ({ movie, selected, handleLikeTouch, likedText }) => {

    /**
     * 
     * @param {*} string 
     * @returns truncated string if its length is grater than 24
     */
    const truncateString = (string) => {
        if (string?.length > 24) {
            const newString = string.substring(0, 24) + ".."
            return newString
        } else {
            return string
        }

    }

    return (
        <CardWrapper>
            <MovieThumbnail source={{ uri: movie.Poster }} />
            <MovieTitle>{truncateString(movie.Title)} </MovieTitle>
            <ReleaseYear>{"("}{movie.Year}{")"}</ReleaseYear>
            <MovieDetail>
                <Director>Genre: {movie.Type}</Director>
            </MovieDetail>
            <AddToFavouraite onPress={() => handleLikeTouch(movie.Title)}>
                <MaterialIcons
                    name={`${selected ? "favorite" : "favorite-outline"}`}
                    size={30}
                    style={{
                        margin: 5,
                        color: selected ? "seagreen" : 'seagreen',
                    }}
                />
                <AddToFavouraiteText> {likedText || "Add To Playlist"} </AddToFavouraiteText>
            </AddToFavouraite>

        </CardWrapper>
    );
};

export default MoviesCard;
