import { combineReducers } from "redux";
import movieReducer from "./movie/movieReducer";
import playlistReducer from "./playlist/playlistReducer";


const combinedReducer = combineReducers({
    movieData: movieReducer,
    playlistData: playlistReducer
})

export default combinedReducer