import axios from 'axios';

export const fetchMovies = () => dispatch => {
    dispatch({ type: "LOADING", payload: null })
    axios.get('https://www.omdbapi.com/?s=Batman&page=2&apikey=317fa99b').then(res => {
        dispatch({ type: "MOVIES_FETCHED", payload: res.data.Search })
    })
}