const initialState = {
    movies: [],
    isFetching: false
}

const movieReducer = (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case "LOADING":
            return {
                ...state,
                isFetching: true
            }
        case "MOVIES_FETCHED":
            return {
                ...state,
                isFetching: false,
                movies: payload
            }
        case 'FAILURE':
            return {
                ...state,
                isFetching: false,
            }
        default:
            return state
    }
}

export default movieReducer