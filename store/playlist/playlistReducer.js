
const initialState = {
    movies: [],
}

const playlistReducer = (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case "GET_FROM_STORAGE":
            return {
                ...state,
                movies: payload
            }
        case "ADD_TO_PLAYLIST":
            return {
                ...state,
                movies: [...state.movies, payload]
            }
        case "REMOVE_FROM_PLAYLIST":
            return {
                ...state,
                movies: state.movies.filter(movie => movie.Title !== payload)
            }

        default:
            return state
    }
}

export default playlistReducer