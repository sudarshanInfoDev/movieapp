
export const addToPlaylist = (data) => dispatch => {
    dispatch({ type: "ADD_TO_PLAYLIST", payload: data })
}
export const removeFromPlaylist = (id) => dispatch => {
    dispatch({ type: "REMOVE_FROM_PLAYLIST", payload: id })
}