import AsyncStorage from '@react-native-async-storage/async-storage';

export const saveToStorage = (data) => {
    try {
        const jsonValue = JSON.stringify(data)
        AsyncStorage.setItem('@redux-data', jsonValue)
    } catch (e) {
        console.log(e, "error on save")

    }
}

export const getDataFromStorage = async (dispatch) => {
    try {
        const jsonValue = await AsyncStorage.getItem('@redux-data')

        console.log(jsonValue)

        if (jsonValue !== null) {
            dispatch({
                type: "GET_FROM_STORAGE",
                payload: JSON.parse(jsonValue)
            })
            console.log(JSON.parse(jsonValue))
        }
    } catch (e) {
        // error reading value
        console.log(e, "error on get")

    }
}
