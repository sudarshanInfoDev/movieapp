
import axios from 'axios';
import React, { useState } from 'react';
import { useEffect } from 'react';
import {
    Dimensions,
    SafeAreaView,
    ScrollView,
    ActivityIndicator
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components/native';
import Header from '../../components/Header';
import MoviesCard from '../../components/MovieCard';
import { fetchMovies } from '../../store/movie/movieAction';
import { addToPlaylist } from '../../store/playlist/playlistAction';

const { width, height } = Dimensions.get("window");
const HomeWrapper = styled.View`
    flex:1;
    padding: 0px 10px;
    display: flex;
    flex-direction:row;
    flex-wrap:wrap;
    justify-content: space-around;
    align-items:center;
`;
const IndicatorWrapper = styled.View`
    flex:1;
    justify-content:center;
    margin-top: ${height / 2}px;
`;

const Home = () => {
    const dispatch = useDispatch()
    const movieList = useSelector(state => state.movieData.movies)
    const isFetching = useSelector(state => state.movieData.isFetching)
    const shortList = useSelector(state => state.playlistData.movies)

    useEffect(() => {
        dispatch(fetchMovies())
    }, [])


    /**
     * check if the given movie is in Playlist
     * @param {*} title 
     * @returns true | false
     */
    const presentInPlaylist = (title) => {
        if (shortList.some(movie => movie.Title === title)) {
            return true
        } else {
            return false
        }
    }


    /**
     * @param title if movie title already exists remove it else add to array 
     */
    const handleLikeTouch = (title) => {
        if (!shortList.some(movie => movie.Title === title)) {
            const sendData = movieList.find(movie => movie.Title === title)
            dispatch(addToPlaylist(sendData))
        }
    }


    return (
        <SafeAreaView style={{ backgroundColor: '#ddd', width: width, flex: 1 }}>
            <Header />
            <ScrollView contentInsetAdjustmentBehavior="automatic">
                <HomeWrapper>
                    {
                        isFetching
                            ? <IndicatorWrapper>

                                <ActivityIndicator size={80} color="seagreen" style={{
                                    textAlign: 'center',

                                }} />
                            </IndicatorWrapper>

                            : movieList && movieList.map(item => (
                                <React.Fragment key={item.Title}>
                                    <MoviesCard
                                        movie={item}
                                        selected={presentInPlaylist(item.Title)}
                                        handleLikeTouch={handleLikeTouch}
                                        likedText={presentInPlaylist(item.Title) ? "In Playlist" : "Add To Playlist"}
                                    />
                                </React.Fragment>
                            ))
                    }
                </HomeWrapper>

            </ScrollView>
        </SafeAreaView>
    );
};

export default Home;
