
import React, { useState } from 'react';
import {
    Dimensions,
    SafeAreaView,
    ScrollView
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components/native';
import Header from '../../components/Header';
import MoviesCard from '../../components/MovieCard';
import { removeFromPlaylist } from '../../store/playlist/playlistAction';

const { width } = Dimensions.get("window");
const HomeWrapper = styled.View`
    flex:1;
    padding: 0px 10px;
    display: flex;
    flex-direction:row;
    flex-wrap:wrap;
    justify-content: space-around;
`;
const NoMoviesText = styled.Text`
    font-size: 18px;
    margin-top: 25px;
    font-weight: bold;
    text-transform:uppercase;
`;

const ShortList = () => {
    const dispatch = useDispatch()

    const playlist = useSelector(state => state.playlistData.movies)


    /**
     * @param title if movie title already exists remove it else add to array 
     */
    const handleLikeTouch = (title) => {
        dispatch(removeFromPlaylist(title))
    }



    return (
        <SafeAreaView style={{ backgroundColor: '#ddd', width: width, flex: 1 }}>
            <Header title="YOUR PLAYLIST" icon="format-list-checks" />
            <ScrollView contentInsetAdjustmentBehavior="automatic">
                <HomeWrapper >
                    {
                        playlist.length > 0 ? playlist.map(item => (
                            <React.Fragment key={item.Title}>
                                <MoviesCard
                                    movie={item}
                                    selected={true}
                                    handleLikeTouch={handleLikeTouch}
                                    likedText="Remove "
                                />
                            </React.Fragment>
                        ))
                            : <HomeWrapper>
                                <NoMoviesText>You have no movies in playlist</NoMoviesText>
                            </HomeWrapper>
                    }
                </HomeWrapper>
            </ScrollView>
        </SafeAreaView>
    );
};

export default ShortList;
